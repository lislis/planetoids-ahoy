export class Planetoid {
    name;
    neo_reference_id;
    nasa_jpl_url;
    absolute_magnitude_h;
    is_potentially_hazardous_asteroid;
}
